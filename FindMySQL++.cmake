# - Try to find MySQL++
# Once done this will define
#  MySQL++_FOUND - System has MySQL++
#  MySQL++_INCLUDE_DIRS - The MySQL++ include directories
#  MySQL++_LIBRARIES - The libraries needed to use MySQL++

find_path(MySQL++_INCLUDE_DIR mysql++.h PATHS /usr/local/Cellar/mysql++/3.2.0/include PATH_SUFFIXES mysql++)

find_library(MySQL++_LIBRARY mysqlpp PATHS /usr/local/Cellar/mysql++/3.2.0/lib)

set(MySQL++_LIBRARIES ${MySQL++_LIBRARY})
set(MySQL++_INCLUDE_DIRS ${MySQL++_INCLUDE_DIR})

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set MySQL++_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(MySQL++  DEFAULT_MSG
                                  MySQL++_LIBRARY MySQL++_INCLUDE_DIR)

mark_as_advanced(MySQL++_INCLUDE_DIR MySQL++_LIBRARY )
